function each(elements, cb) {
    for (let index=0;index<elements.length;index++){
        let val = elements[index]
        cb(val,index)
    }
}

module.exports = each