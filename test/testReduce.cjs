const items = [1, 2, 3, 4, 5, 5];

const reduceResult = require('../reduce.cjs')

let ans = reduceResult(items,((acc,item)=> acc+item))
let withStringValue = reduceResult(items,((acc,item)=> acc+item),2)

console.log(ans)
console.log(withStringValue)