const nestedArray = [1, [2], [[3]], [[[4]]]];

const arr4 = [1, 2, [3, , [5, 6, [7, 8, [9, 10]]]]];

const flattenResult = require('../flatten.cjs')


console.log(flattenResult(nestedArray))
console.log(nestedArray.flat())


console.log(flattenResult(arr4))
console.log(arr4.flat())