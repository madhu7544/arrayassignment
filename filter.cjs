function filter(elements, cb) {
    // Do NOT use .filter, to complete this function.
    // Similar to `find` but you will return an array of all elements that passed the truth test
    // Return an empty array if no elements pass the truth test
    let newArray =[];
    for (let ind=0;ind<elements.length;ind++){
        if (cb(elements[ind], ind, elements) ===true){
            newArray.push(elements[ind])
        }
    }
    return newArray

}

module.exports=filter 