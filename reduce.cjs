function reduce(elements, cb, startingValue) {
    let acc;
    if (startingValue !== undefined){
        acc = startingValue
    }else{
        acc = elements[0]
    }
    for (let ind=0;ind<elements.length;ind++){
        if (startingValue!==undefined || ind >0 ){
            acc = cb(acc,elements[ind],ind,elements)
        }
    }
    return acc
}

module.exports= reduce