function map(elements, cb) {
    let newArray =[];
    for (let ind=0;ind<elements.length;ind++){
        let num = (elements[ind])
        newArray.push(cb(num,ind,elements))
    }
    return newArray
}

module.exports = map