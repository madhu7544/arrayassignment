
function flatten(elements,depth) {
    let flatArray = [];
    if (depth===undefined){
        depth=1;
    
    }

    for (let ind=0;ind<elements.length;ind++){
        if (Array.isArray(elements[ind]) && depth >0 ){
            flatArray = flatArray.concat(flatten(elements[ind],depth-1))
        }
        else if (elements[ind] !== undefined){
            flatArray.push(elements[ind])
        }
    }
    return flatArray;
}

module.exports = flatten
